import pygame
from App.GameScene import GameScene
from Object.Circle import Circle
from Object.Object import Object
from Object.Line import Line
from Math.Vec2 import Vec2
from Object.Polygon import Polygon
from Object.Joint import Joint
from Projects.HillDriving.Car import Car
from Color import Color
from Math import Math
import math
import random
import datetime
from Math import PhysicWorker
import Config

class GameplayScene(GameScene):
    def __init__(self):
        super().__init__()
        random.seed( datetime.datetime.now() )
        self.should_toggle_pause = False
        self.map_length = 100
        self.createMap(self.map_length)
        pass
    
    def update(self,duration):
        super().update(duration)

        for i in range( len(self.objects_array) ):
            for j in range (i+1 , len(self.objects_array) ):
                PhysicWorker.CollisionResponde.ResolveCollision( self.objects_array[i], self.objects_array[j] )

        pass

    def render(self):
        super().render()

        self.setCamera( self.car.position )
        
        pass

    def onClick(self, x, y):
        self.car.moveRight()

    def onHoldLeftMouse(self,x,y):
        pass

    def onHoldRightMouse(self,x,y):
        pass

    def onDDown(self):
        self.car.moveRight()

    def onADown(self):
        self.car.moveLeft()

    def createMap(self,length):
        self.map=[]
        self.map.append(50)
        self.map.append(50)
        for i in range(length-2):
            self.map.append( random.randint( 0, 200 ) )

        ##### Add objects
        line = Line( Color.RED, 0, self.map[0], 0,  self.map[0]-300 )
        self.addObject(line)

        for i in range(length-1):
            line = Line( Color.RED, i*200, self.map[i], (i+1)*200, self.map[i+1] )
            self.addObject(line)
    
        self.car = Car(Color.WHITE, 100,0)
        self.camera.setFollow( self.car )
        self.addObject( self.car )

    def onESCDown(self):
        self.should_toggle_pause = True
        return
        
    def onESCUp(self):
        if (self.should_toggle_pause):
            from App.GameManager import GameManager
            GameManager.getInstance().togglePause()
            self.should_toggle_pause = False   
        return
        
    def restart(self):
        self.objects_array.clearAll()
        self.createMap(self.map_length)



