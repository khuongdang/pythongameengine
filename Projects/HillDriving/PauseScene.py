import pygame
from App.GameScene import GameScene
import Config
from Color import Color
from UI.Button import Button
from Math.Vec2 import Vec2

class PauseScene(GameScene):
    def __init__(self):
        super().__init__()

        self.should_unpause = False

        middle_scene = Vec2(self.size[0],self.size[1])/2

        self.resume_button = Button(Color.RED,"Resume", (middle_scene) ,Vec2(200,90))
        self.resume_button.setBorderColor(Color.WHITE)
        self.resume_button.onclick = self.unpause
        self.addGUIElement( self.resume_button )

        self.restart_button = Button(Color.RED,"Restart", (middle_scene + Vec2(0,100)) ,Vec2(200,90))
        self.restart_button.setBorderColor(Color.WHITE)
        self.restart_button.onclick = self.restart
        self.addGUIElement( self.restart_button )

        self.exit_button = Button(Color.RED,"Exit", (middle_scene + Vec2(0,200)) ,Vec2(200,90))
        self.exit_button.setBorderColor(Color.WHITE)
        self.exit_button.onclick = self.exit
        self.addGUIElement( self.exit_button )


    def render(self):
        location = (self.size[0]/2, 300)
        self.blurBackground()
        self.renderText( "PAUSED", location,size= 100 )

        super().render()

    def restart(self):
        from App.GameManager import GameManager
        GameManager.getInstance().restart()
        return

    def exit(self):
        from App.GameManager import GameManager
        GameManager.getInstance().returnToMainMenu()
        return

    def unpause(self):
        from App.GameManager import GameManager
        GameManager.getInstance().unpause()
        return

    def onHover(self, x, y):
        return super().onHover(x, y)

    def onClick(self, x, y):
        return super().onClick(x, y)

    def onESCDown(self):
        self.should_unpause = True
        return

    def onESCUp(self):
        if (self.should_unpause):
            self.unpause()
        return

