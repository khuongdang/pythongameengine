import pygame
from App.GameScene import GameScene
import Config
from Color import Color
from UI.Button import Button
from Math.Vec2 import Vec2

class MenuScene(GameScene):
    def __init__(self):
        super().__init__()

        middle_scene = Vec2(self.size[0],self.size[1])/2

        self.play_button = Button(Color.RED,"New Game", (middle_scene) ,Vec2(200,90))
        self.play_button.setBorderColor(Color.WHITE)
        self.play_button.onclick = self.startGame
        self.addGUIElement( self.play_button )

        self.option_button = Button(Color.RED,"Option", (middle_scene) + Vec2 (0,100) ,Vec2(200,90))
        self.option_button.setBorderColor(Color.WHITE)
        self.option_button.onclick = self.option
        self.option_button.disable()
        self.addGUIElement( self.option_button )

        self.exit_button = Button(Color.RED,"Exit", (middle_scene) + Vec2 (0,200) ,Vec2(200,90))
        self.exit_button.setBorderColor(Color.WHITE)
        self.exit_button.onclick = self.showExitPromp
        self.addGUIElement( self.exit_button )

        self.confirm_exit_button = Button(Color.GREEN,"Quit", (middle_scene) + Vec2 (-110,0) ,Vec2(200,90))
        self.confirm_exit_button.setBorderColor(Color.WHITE)
        self.confirm_exit_button.onclick = self.exit
        self.addDialogElement( self.confirm_exit_button )

        self.cancle_exit_button = Button(Color.RED,"Cancel", (middle_scene) + Vec2 (110,0) ,Vec2(200,90))
        self.cancle_exit_button.setBorderColor(Color.WHITE)
        self.cancle_exit_button.onclick = self.gui.hideDialog
        self.addDialogElement( self.cancle_exit_button )

        self.gui.setDialogPosition( (middle_scene) + Vec2(0,-100) )
        self.gui.setDialogText("Do you wanna quit?",size= 100)

        self.shouldToggleDialog = True

    def render(self):
        middle_scene = Vec2(self.size[0],self.size[1])/2
        self.renderText("HILL DRIVING", middle_scene + Vec2(0,-200),size= 100)

        return super().render()

    def startGame(self):
        from App.GameManager import GameManager
        GameManager.getInstance().startGame()
        return

    def onESCDown(self):
        if self.shouldToggleDialog:
            self.gui.toggleDialog()
            self.shouldToggleDialog = False
        return

    def onESCUp(self):
        self.shouldToggleDialog = True
        return

    def showExitPromp(self):
        self.gui.showDialog()
        return

    def exit(self):
        from App.GameManager import GameManager
        GameManager.getInstance().exit()
        return

    def option(self):
        print("option")
        return

