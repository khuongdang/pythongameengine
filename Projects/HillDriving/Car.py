import pygame
from Color import Color
from Math.Vec2 import Vec2
from Object.ObjectsGroup import ObjectsGroup
from Object.Object import Object
from Object.Circle import Circle
from Object.Joint import Joint
from Object.Polygon import Polygon

class Car(ObjectsGroup):

    def __init__(self, color, left, top):

        super().__init__()

        self.color = color
        self.position = Vec2(left,top)
        self.accelerator = Vec2.Zero()

        self.car_length = 130
        self.car_height = 40
        self.wheels_distance = 80
        self.wheel_radius = 15
        self.wheel_body_height =5
        self.driver_head_radius = 10
        self.driver_head_height = 10
        self.is_moving_right = False
        self.is_moving_left = False

        self.engine_accelerator = 70

        self.createCar()

    def createCar(self):
        #### Create Wheels
        self.leftWheels = Circle(self.color, self.position.x - self.wheels_distance/2, self.position.y, self.wheel_radius)
        self.leftWheels.plasticity = 0.8
        self.rightWheels = Circle(self.color, self.position.x + self.wheels_distance/2, self.position.y, self.wheel_radius) 
        self.rightWheels.plasticity = 0.8

        #### Create Body

        vertexs = [                                              \
            Vec2( -self.car_length/2, 0 ),                      \
            Vec2( -self.car_length/2, -self.car_height ),        \
            Vec2( self.car_length/2, -self.car_height ),         \
            Vec2( self.car_length/2, 0 )                        \
        ]
        self.body = Polygon(self.color, self.position.x, self.position.y, vertexs)
        self.body.position = self.position + Vec2(0,-self.wheel_body_height).rotate(self.body.angle)
        self.body.gravity = Vec2(0,0)

        ### Create driver
        self.driver = Circle(self.color, self.position.x , self.position.y-self.car_height-self.driver_head_height , self.driver_head_radius)
        self.driver.mass = 20
        #### Create Joint
        self.wheels_joint = Joint( Color.CLEAR )
        self.wheels_joint.addObject(self.leftWheels)
        self.wheels_joint.addObject(self.rightWheels)

        self.driver_head_joint = Joint( self.color )
        self.driver_head_joint.addObject(self.body)
        self.driver_head_joint.addObject(self.driver)

        self.driver_head_right_control_joint = Joint( Color.CLEAR )
        self.driver_head_right_control_joint.addObject(self.rightWheels)
        self.driver_head_right_control_joint.addObject(self.driver)
        self.driver_head_right_control_joint.should_propagate_update = False

        self.driver_head_left_control_joint = Joint( Color.CLEAR )
        self.driver_head_left_control_joint.addObject(self.leftWheels)
        self.driver_head_left_control_joint.addObject(self.driver)
        self.driver_head_left_control_joint.should_propagate_update = False

        ####
        self.addObject( self.wheels_joint )  
        self.addObject( self.driver_head_joint )
        self.addObject( self.driver_head_right_control_joint )
        self.addObject( self.driver_head_left_control_joint )

    def render(self):
        super().render()
        # from App.GameManager import GameManager
        # GameManager.getInstance().addRenderQueue(  "Circle", [ self.position+self.camera.getScroll(), self.camera.bounding_circle ] )
        #pygame.draw.rect(self.surface, Color.RED, pygame.Rect( self.position, self.size ) )
        # #car body
        # pygame.draw.rect(self.surface, self.color, pygame.Rect( self.position, self.size ) )

        # #left wheel holder
        # pygame.draw.circle(self.surface, Color.CLEAR , [int(self.position.x + distance_to_wheel ), int(self.position.y + height)] , radius + 5 )

        # #left wheel
        # pygame.draw.circle(self.surface, self.color, [int(self.position.x + distance_to_wheel ), int(self.position.y + height)] , radius )
        
        # #right wheel holder
        # pygame.draw.circle(self.surface, Color.CLEAR , [int(self.position.x + width - distance_to_wheel ), int(self.position.y + height)] , radius + 5 )

        # #right wheel
        # pygame.draw.circle(self.surface, self.color, [int(self.position.x + width - distance_to_wheel ), int(self.position.y + height)] , radius  )
        # #pygame.draw.rect(self.image, self.color, [0,0,self.rect.width , self.rect.height])

    def update(self, duration):
        self.resolveMoveCommand()

        super().update(duration)
        self.position = self.wheels_joint.position
        self.body.angle = self.wheels_joint.angle
        self.body.position = self.position + Vec2(0,-self.wheel_body_height).rotate(self.body.angle)

    def resolveMoveCommand(self):
        if (self.is_moving_right):
            if (self.leftWheels.is_touched or self.leftWheels.is_grounded ):
                self.leftWheels.accelerate( Vec2(self.engine_accelerator,0) )
            if (self.rightWheels.is_touched or self.rightWheels.is_grounded ):
                self.rightWheels.accelerate( Vec2(self.engine_accelerator,0) )

        elif (self.is_moving_left):
            if (self.leftWheels.is_touched or self.leftWheels.is_grounded ):
                self.leftWheels.accelerate( Vec2(-self.engine_accelerator,0) )
            if (self.rightWheels.is_touched or self.rightWheels.is_grounded ):
                self.rightWheels.accelerate( Vec2(-self.engine_accelerator,0) )

        self.is_moving_right = False
        self.is_moving_left = False

    def moveRight(self, _bool = True):
        self.is_moving_right = _bool

    def moveLeft(self, _bool = True):
        self.is_moving_left = _bool
            
        
        
        
        