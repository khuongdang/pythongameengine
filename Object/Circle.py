import pygame
from Color import Color
from Math import Math
from Math.Vec2 import Vec2
from Object.Object import Object

class Circle(Object):

    def __init__(self, color, left, top, radius = 20):

        super().__init__()

        self.color = color
        self.position = Vec2(left,top)
        self.radius = radius
        self.mass = Math.Square(self.radius) * Math.PI
        self.accelerator = Vec2( 0 , 0 )

    def render(self):
        super().render()
        #Draw Circle
        pygame.draw.circle(self.surface , self.color , 
            [int(self.position.x) + int(self.camera.getScroll().x),      \
            int(self.position.y) + int(self.camera.getScroll().y)],       \
        int(self.radius))

        pass

    def update(self, duration):
        super().update(duration)
        pass
        