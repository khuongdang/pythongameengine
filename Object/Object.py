import pygame
import Definition
import Config
from Math import Math
from Math.Vec2 import Vec2

class Object(pygame.sprite.Sprite):

    def __init__(self):

        self.surface = pygame.display.get_surface()
        self.gravity = Config.GRAVITY
        self.max_speed = Definition.MAX_SPEED
        self.position = Vec2.Zero()
        self.accelerator = Vec2.Zero()
        self.velocity = Vec2.Zero()
        self.plasticity = 1.0
        self.mass = 100.0
        self.friction_strengh = Config.FRICTION
        self.is_movable = True
        self.is_touched = False
        self.is_grounded = False
        self.applying_force = Vec2.Zero()
        self.angle = 0
        self.angle_velocity = 0
        self.camera = None

    def setCamera(self, camera):
        from App.Camera import Camera
        if (not isinstance(camera,Camera) ):
            return False
        self.camera = camera
        return True

    def shift(self, Vec2):
        self.position += Vec2
        return self

    def accelerate(self, Vec2):
        self.velocity += Vec2 
        return self

    def reflect(self, Vec2):
        self.velocity = self.velocity.reflect(Vec2) * self.plasticity
        return self

    def friction(self, strengh):
        self.velocity = self.velocity.friction(strengh)
        return self

    def touched(self, isTouched = True):
        self.is_touched = isTouched

    def update(self, duration):
        if (self.is_movable):
            self.accelerate((self.accelerator+self.gravity) * duration)

            # UPDATE VELOCITY
            if (self.max_speed != None):
                if ( self.velocity.x >= self.max_speed ):
                    self.velocity.x = self.max_speed
                if (self.velocity.y >= self.max_speed):
                    self.velocity.y = self.max_speed

            if ( self.is_touched ):
                self.velocity = self.velocity.friction( self.friction_strengh )
                self.is_touched = False

            # UPDATE VELOCITY

            self.shift(self.velocity * duration)

        ## calculate angle
        self.angle += self.angle_velocity * duration
        if( self.angle >= 360 ):
            self.angle -= 360
        elif (self.angle <= 0 ):
            self.angle += 360

        ##calculate angle
        
        return self

    def render(self):

        return self

    def shouldRender(self):
        if( self.camera != None ):
            distance = Math.DistanceFromPoints(self.camera.getPosition(), self.position)
            if( distance > self.camera.bounding_circle):
                return False
        return True

    def destroy(self):
        del self
        return