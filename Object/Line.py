import pygame
from Color import Color
from Math import Math
from Math.Vec2 import Vec2
from Object.Object import Object

class Line(Object):

    def __init__(self, color, left, top, right, bottom, radius = 10):

        super().__init__()

        self.color = color
        self.first_point = Vec2(left,top)
        self.second_point = Vec2(right,bottom)
        self.radius = radius
        self.mass = Math.Square(self.radius) * Math.PI
        self.accelerator = Vec2( 0 , 0 )
        self.is_movable = False
        self.position = Vec2( self.first_point/2 + self.second_point/2 )

    def getVector(self):
        return Math.GetVecFromPoints( self.first_point, self.second_point )

    def render(self):
        super().render()
        scroll = self.camera.getScroll()
        #Draw Circle
        pygame.draw.circle(self.surface, self.color , [int(self.first_point.x + scroll.x ), int(self.first_point.y + scroll.y)] , int(self.radius))
        pygame.draw.circle(self.surface, self.color , [int(self.second_point.x + scroll.x), int(self.second_point.y + scroll.y)] , int(self.radius))

        #calculate border draw position
        line_vector = self.getVector()
        line_tangent_vector =  Vec2( -line_vector.y, line_vector.x )

        line_tangent_vector = line_tangent_vector.normalize() * self.radius

        pygame.draw.line( self.surface, self.color, self.first_point+line_tangent_vector+scroll, self.second_point+line_tangent_vector+scroll )
        pygame.draw.line( self.surface, self.color, self.first_point-line_tangent_vector+scroll, self.second_point-line_tangent_vector+scroll )
        pass

    def update(self, duration):
        super().update(duration)
        pass
        