import pygame
from Color import Color
from Math import Math
from Math.Vec2 import Vec2
from Object.Object import Object

class ObjectsGroup(Object):

    def __init__(self):
        super().__init__()
        self.gravity = Vec2.Zero()
        self.max_speed = None
        self.position = Vec2.Zero()
        self.accelerator = Vec2.Zero()
        self.velocity = Vec2.Zero()
        self.plasticity = 1.0
        self.mass = 0
        self.friction_strengh = 0
        self.is_movable = True
        self.is_touched = False
        self.should_propagate_update = True

        self.objects = []

    def update(self, duration):
        if( self.should_propagate_update ):
            for _object in self.objects:
                _object.update(duration)
        return

    def render(self):
        super().render()
        for _object in self.objects:
            _object.render()
        return

    def setCamera(self, camera):
        self.camera = camera
        for _object in self.objects:
            _object.setCamera(camera)
        return True

    def addObject(self, _object):
        _object.setCamera(self.camera)
        self.objects.append(_object)
