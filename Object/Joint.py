import pygame
import math
from Color import Color
from Math import Math
from Math.Vec2 import Vec2
from Object.ObjectsGroup import ObjectsGroup

class Joint(ObjectsGroup):
    def __init__(self, color):
        super().__init__()

        self.color = color
        self.damper = 0.3

        self.fixed_distance = 0

        self.objects = [None,None]

    def addObject(self, Object):
        if (self.objects[0] == None ):
            Object.setCamera( self.camera )
            self.objects[0] = Object
        elif (self.objects[1] == None):
            Object.setCamera( self.camera )
            self.objects[1] = Object
            self.fixed_distance = Math.DistanceFromPoints( self.objects[0].position, self.objects[1].position )
            self.position = ( self.objects[0].position + self.objects[1].position )/2
        else:
            return False
        return True

    def update(self, duration):
        vector = self.getVector()
        self.angle = math.degrees(math.atan2(vector.y,vector.x))
        self.position = ( self.objects[0].position + self.objects[1].position )/2

        if ( (self.objects[0] != None) and (self.objects[1] != None) ):
            ### TODO: Fix velocity and moving distance base on mass
            mass_ratio = math.pow(self.objects[0].mass,2)/pow( (self.objects[0].mass + self.objects[1].mass),2 )
            distance = self.getDistance()
            move_dis = (distance - self.fixed_distance)
            move_vec = self.getVector()
            if ( move_vec != Vec2.Zero() ):
                self.objects[0].shift( ( move_vec.normalize() * move_dis )*self.damper*(1-mass_ratio) )
                self.objects[1].shift( ( -move_vec.normalize() * move_dis )*self.damper*(mass_ratio) )
                self.objects[0].velocity += ( move_vec.normalize() * move_dis ) * (1- self.damper) *(1-mass_ratio)
                self.objects[1].velocity += ( -move_vec.normalize() * move_dis ) * (1- self.damper) * (mass_ratio)

        super().update(duration)

    def render(self):
        if ( (self.objects[0] != None) and (self.objects[1] != None) ):
            super().render()
            scroll = self.camera.getScroll()
            if(self.color != Color.CLEAR):  
                pygame.draw.line( self.surface, self.color, self.objects[0].position + scroll, self.objects[1].position + scroll )

    def getDistance(self):
        if ( (self.objects[0] != None) and (self.objects[1] != None) ):
            return Math.DistanceFromPoints( self.objects[0].position, self.objects[1].position )
        return 0

    def getVector(self):
        if ( (self.objects[0] != None) and (self.objects[1] != None) ):
            return Math.GetVecFromPoints( self.objects[0].position, self.objects[1].position )
        return Vec2.Zero()

    
