import pygame
from Color import Color
from Math import Math
from Math.Vec2 import Vec2
from Object.Object import Object

class Polygon(Object):

    def __init__(self, color, left, top, vertexs = [], mass = 300):

        super().__init__()
        
        self.color = color
        self.position = Vec2(left,top)
        self.vertexs = vertexs
        self.mass = mass
        # self.highlight = [0,1,2,3]
        
        ##Calculate Bounding Box 
        max_mag = 0
        for i in range( 0, len( self.vertexs )-1 ):
            line_length = vertexs[i].magnitude()
            if line_length > max_mag:
                max_mag = line_length

        self.bounding_radius = max_mag


    def getVertexPos(self,vertex_num):
        if( vertex_num >= len( self.vertexs ) ):  
            return False
        if( vertex_num < -len(self.vertexs) ):
            return False

        return Vec2( self.position + self.vertexs[ vertex_num ].rotate( self.angle ) )

    def render(self):
        super().render()
        scroll = self.camera.getScroll()
        for i in range(0, len( self.vertexs ) - 1 ):
            color = self.color
            # if ( i in self.highlight ):
            #     color = Color.GREEN
            pygame.draw.line( self.surface, color,                          \
                self.position + scroll + self.vertexs[i].rotate(self.angle)  ,             \
                self.position + scroll + self.vertexs[i+1].rotate(self.angle)              \
                )
        if( len(self.vertexs) >= 3 ):
            color = self.color
            # if ( len(self.vertexs)-1 in self.highlight or -1 in self.highlight ):
            #     color = Color.GREEN
            pygame.draw.line( self.surface, color,                          \
            self.position + scroll + self.vertexs[-1].rotate(self.angle) ,                 \
            self.position + scroll + self.vertexs[0].rotate(self.angle)                    \
            )            

        # add bounding circle
        # pygame.draw.circle(self.surface, self.color , [int(self.position.x), int(self.position.y)] , int(self.bounding_radius), 2)      
        pass

    def update(self, duration):
        super().update(duration)
        pass
        