import pygame
from Color import Color
import time
from App.GameManager import GameManager 
import Config

second_per_frame = Config.second_per_frame

class Application:

    def main(self):
        pygame.init()
        pygame.display.set_caption("Hill Driving")

        last_render_time = time.time()
        last_update_time = last_render_time

        self.game = GameManager( Config.width, Config.height, Config.prod )

        #init scene
        self.game.initScenes()
        running = True

        while running:

            if pygame.event.peek():
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        running = False
                    if event.type == pygame.ACTIVEEVENT:
                        pass
                    if event.type == pygame.VIDEORESIZE:
                        width, height = event.w, event.h
                        self.game.setSize(width,height)
                        pass

            now = time.time()
            from_last_update = now - last_update_time      
            
            #Update small time at the time if the game is lag behide
            while ( from_last_update > 0.03):           
                self.game.update(0.03)
                from_last_update -= 0.03
            self.game.update( from_last_update )

            last_update_time = now

            now = time.time()
            from_last_frame = now - last_render_time
            if( from_last_frame >= second_per_frame ):
                self.game.render()
                last_render_time = now    

        pygame.quit()

if __name__ == "__main__":
    app = Application()
    app.main()