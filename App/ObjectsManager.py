from Object.Object import Object

class ObjectsManager:
    def __init__(self):
        self.objects_array = []

    def __getitem__(self, index):
        return self.objects_array[index]

    def __len__(self):
        return len(self.objects_array)

    def addObject(self, obj):
        if ( isinstance(obj, Object) ):
            obj.setCamera( self.camera )
            self.objects_array.append( obj )

    def setCamera(self, camera):
        self.camera = camera
        for _object in self.objects_array:
            _object.setCamera( camera )

    def render(self):
        for _object in self.objects_array:
            if (_object.shouldRender() ):
                _object.render()

    def update(self,duration):
        for _object in self.objects_array:
            _object.update(duration)
        pass

    def clearAll(self):
        del self.objects_array[:]
    