from Device.Mouse import Mouse
from Device.Keyboard import Keyboard

class DeviceManager:
    def __init__(self):
        self.mouse = Mouse()
        self.keyboard = Keyboard()
        pass

    def update(self, Manager):
        self.mouse.update()
        self.keyboard.update()


        #### Resolve Mouse
        if( self.mouse.isLeftMouseUp() ):
            Manager.onClick( self.mouse.pos.x, self.mouse.pos.y )
        if( self.mouse.isRightMouseUp() ):
            Manager.onRightClick( self.mouse.pos.x, self.mouse.pos.y )

        #### Resolve Keyboard
        if( self.keyboard.isDDown()):
            Manager.onDDown()
        if( self.keyboard.isADown()):
            Manager.onADown()
        if(self.keyboard.isESCDown()):
            Manager.onESCDown()
        if(self.keyboard.isESCUp()):
            Manager.onESCUp()

        Manager.onHover(self.mouse.pos.x, self.mouse.pos.y)
