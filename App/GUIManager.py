import pygame
from Color import Color
import Config

class GUIManager():
    def __init__(self, size):
        self.size = size
        self.surface = pygame.display.get_surface() 
        self.elements = []
        self.camera = None
        self.dialog_elements = []
        self.show_confirm_dialog = False

        #TODO: Make an Text object
        self.dialog_text = ""
        self.dialog_text_size = 30
        self.dialog_font = "Time New Roman"
        self.dialog_color = Config.DEFAULT_TEXT_COLOR
        self.dialog_text_pos = self.size/2
        pass

    def setSize(self, size):
        self.size = size
        pass
    
    def update(self, duration):
        pass

    def setDialogText(self, text, color = Config.DEFAULT_TEXT_COLOR, size = 30, font = "Time New Roman",pos =  None ):
        self.dialog_text = text
        self.dialog_color = color
        self.dialog_text_size = size 
        self.dialog_font = "Time New Roman"
        if pos is not None:
            self.dialog_text_pos = pos
        return

    def setDialogPosition(self, pos):
        self.dialog_text_pos = pos
        return

    def addGUIElement(self, ele):
        ele.setCamera( self.camera )
        self.elements.append( ele )
        return

    def addDialogElement(self,ele):
        ele.setCamera( self.camera )
        self.dialog_elements.append( ele )
        return

    def setCamera(self, camera):
        self.camera = camera
        for ele in self.elements:
            ele.setCamera( camera )
        for ele in self.dialog_elements:
            ele.setCamera( camera )
        return

    def render(self):
        for ele in self.elements :
            ele.render()

        if( self.show_confirm_dialog ):
            self.blurBackground()

            self.renderText(self.dialog_text, self.dialog_text_pos,self.dialog_color,self.dialog_text_size)

            for ele in self.dialog_elements:
                ele.render()
        return

    def onClick(self,x,y):
        for ele in self.elements :
            ele.onClick(x,y)
        for ele in self.dialog_elements:
            ele.onClick(x,y)
        return

    def onHover(self,x,y):
        for ele in self.elements :
            ele.onHover(x,y)
        for ele in self.dialog_elements :
            ele.onHover(x,y)
        return

    def showDialog(self):
        self.show_confirm_dialog = True
        return

    def hideDialog(self):
        self.show_confirm_dialog = False
        return

    def toggleDialog(self):
        self.show_confirm_dialog = not self.show_confirm_dialog
        return

    def blurBackground(self, size = None, color = Color.BLACK , alpha = Config.PAUSE_SCENE_TRANSPARENCY):
        if (size is None):
            size = self.size
            surface = pygame.Surface( size )
            surface.fill( color )
            surface.set_alpha( alpha )
            self.surface.blit( surface, (0,0) )

    def renderText(self, text, location,color = Color.WHITE, size = 30, font = 'Time New Roman', align = "center"):
        pygame.font.init()
        myfont = pygame.font.SysFont(font,size)
        textsurface = myfont.render(text,True,color)
        surface_size = myfont.size(text)

        if (align == "left"):
            pos = ( location[0]  , location[1] - surface_size[1]/2 )
        elif (align == "right"):
            pos = ( location[0] - surface_size[0]  , location[1] - surface_size[1]/2 )
        else:
            pos = ( location[0] - surface_size[0]/2  , location[1] - surface_size[1]/2 )

        self.surface.blit( textsurface, pos )