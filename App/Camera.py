import pygame
import math
import Config
from Math.Vec2 import Vec2
from Object.Object import Object

class Camera:
    def __init__(self, size):
        super().__init__()
        self.follow = None
        self.position = Vec2(0,0)
        self.size = size
        self.bounding_circle = math.sqrt( size.x*size.x/4 + size.y*size.y/4 ) + Config.RENDER_RANGE

    def getPosition(self):
        if (self.follow is not None):
            self.position = self.follow.position
        return self.position
    
    def getScroll(self):
        position = self.getPosition()
        return (self.size/2 - position)
    
    def setFollow(self,_object):
        if ( not isinstance(_object,Object) ):
            return False
        self.follow = _object
        return self.follow

    def setSize(self, size):
        self.size = size
        return
