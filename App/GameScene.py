from Object.Object import Object
from Color import Color
import Config
import pygame
from UI.GUIElement import GUIElement
from App.GUIManager import GUIManager
from Math.Vec2 import Vec2
from App.Camera import Camera
from App.ObjectsManager import ObjectsManager

class GameScene:
    def __init__(self):    
        self.surface = pygame.display.get_surface() 
        self.size = Vec2( Config.width,Config.height )
        self.camera = Camera( self.size )
        self.gui = GUIManager( self.size )
        self.objects_array = ObjectsManager()
        self.objects_array.setCamera( self.camera )
        pass

    def setSize(self, size):
        self.size = size
        self.gui.setSize( size )
        self.camera.setSize( size )
        return
    
    def update(self,duration):
        self.objects_array.update(duration)

    def setCamera(self, vec2):
        self.camera_position = vec2
        pass

    def render(self):
        self.objects_array.render()
        self.gui.render()

    def addObject(self, obj): 
        self.objects_array.addObject(obj)

    def addGUIElement(self, ele):
        if ( isinstance(ele, GUIElement) ):
            ele.setCamera( self.camera )
            self.gui.addGUIElement( ele )
        return

    def addDialogElement(self, ele):
        if ( isinstance(ele, GUIElement) ):
            ele.setCamera( self.camera )
            self.gui.addDialogElement( ele )
        return

    def onClick(self, x, y):
        self.gui.onClick(x,y)
        return

    def onHoldLeftMouse(self,x,y):
        return NotImplementedError  

    def onHoldRightMouse(self,x,y):
        return NotImplementedError
    
    def onDDown(self):
        return NotImplementedError

    def onADown(self):
        return NotImplementedError

    def onHover(self,x,y):
        self.gui.onHover(x,y)
        return

    def renderText(self, text, location,color = Color.WHITE, size = 30, font = 'Time New Roman', align = "center"):
        pygame.font.init()
        myfont = pygame.font.SysFont(font,size)
        textsurface = myfont.render(text,True,color)
        surface_size = myfont.size(text)

        if (align == "left"):
            pos = ( location[0]  , location[1] - surface_size[1]/2 )
        elif (align == "right"):
            pos = ( location[0] - surface_size[0]  , location[1] - surface_size[1]/2 )
        else:
            pos = ( location[0] - surface_size[0]/2  , location[1] - surface_size[1]/2 )

        self.surface.blit( textsurface, pos )

    def blurBackground(self, size = None, color = Color.BLACK , alpha = Config.PAUSE_SCENE_TRANSPARENCY):
        if (size is None):
            size = self.size
            surface = pygame.Surface( size )
            surface.fill( color )
            surface.set_alpha( alpha )
            self.surface.blit( surface, (0,0) )

    def restart(self):
        pass