import pygame
from Color import Color
from Math.Vec2 import Vec2
from App.DeviceManager import DeviceManager
from App.GUIManager import GUIManager
from TestGravityScene import TestGravityScene
from Projects.HillDriving.GameplayScene import GameplayScene
from Projects.HillDriving.PauseScene import PauseScene
from Projects.HillDriving.MenuScene import MenuScene

class GameManager:
    __instance = None

    @staticmethod 
    def getInstance():
        if GameManager.__instance == None:
            raise Exception("Gamemanager is not inited")
        return GameManager.__instance

    def __init__(self, width=800, height=600, prod = False):
        if GameManager.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            self.prod = prod
            self.size =(width,height)
            self.surface = pygame.display.set_mode(self.size, pygame.RESIZABLE)
            self.DeviceManager = DeviceManager()
            self.render_queue = []
            self.is_paused = False
            self.pause_scene = PauseScene()
            self.menu_scene = MenuScene()
            self.is_in_menu = False
            GameManager.__instance = self
        pass
    def pause(self):
        self.is_paused = True

    def unpause(self):
        self.is_paused = False

    def addRenderQueue(self, _type, param= []):
        _list = []
        _list.append(_type)
        _list += param
        self.render_queue.append( _list )
        pass

    def initScenes(self):
        self.Scenes = []
        if(self.prod == False):
            self.Scenes.append ( TestGravityScene() )
        else :
            self.Scenes.append ( GameplayScene() )
        self.activeScene = self.menu_scene
        # self.activeScene = self.Scenes[0]
        pass

    def update(self, duration):
        
        self.DeviceManager.update( self )
        if( not self.is_paused ):
            self.render_queue.clear()
            self.activeScene.update(duration)       
        pass

    def renderingRenderQueue(self):
        for ele in self.render_queue:
            print(ele)
            if ( ele[0] == "Line" ):
                pygame.draw.line( self.surface, Color.WHITE, ele[1], ele[2], 1)
            if ( ele[0] == "Circle"):
                pygame.draw.circle( self.surface, Color.WHITE, ele[1].toTupleInt() , int(ele[2]),1 )
        pass

    def render(self):
        self.surface.fill((0,0,0))

        self.activeScene.render()
        if (self.is_paused):
            self.pause_scene.render()
        
        self.renderingRenderQueue()
        
        # pygame.draw.rect(screen, (0, 128, 255), pygame.Rect(30, 30, 60, 60))
        pygame.display.flip()

    def setSize(self,width,height):
        self.size = Vec2(width,height)
        pygame.display.set_mode(self.size.toTupleInt() , pygame.RESIZABLE)
        self.pause_scene.setSize( self.size )
        self.menu_scene.setSize( self.size )
        for scene in self.Scenes:
            scene.setSize( self.size )

    def onClick( self, x , y ):
        if(not self.is_paused):
            self.activeScene.onClick(x,y)
        else:
            self.pause_scene.onClick(x,y)

    def onHoldRightMouse(self,x,y):
        self.activeScene.onHoldRightMouse(x,y)

    def onHoldLeftMouse(self,x,y):
        self.activeScene.onHoldLeftMouse(x,y)

    def onRightClick (self, x, y):
        # self.is_paused = not self.is_paused
        pass

    def onDDown(self):
        self.activeScene.onDDown()
    
    def onADown(self):
        self.activeScene.onADown()
    
    def onESCDown(self):
        if(not self.is_paused):
            self.activeScene.onESCDown()
        else:
            self.pause_scene.onESCDown()
        return
        
    def onESCUp(self):
        if(not self.is_paused):
            self.activeScene.onESCUp()
        else:
            self.pause_scene.onESCUp()
        return

    def togglePause(self):
        self.is_paused = not self.is_paused
        return
    
    def onHover(self,x,y):
        if(not self.is_paused):
            self.activeScene.onHover(x,y)
        else:
            self.pause_scene.onHover(x,y)
        return

    def startGame(self):
        self.activeScene = self.Scenes[0]
        return

    def restart(self):
        self.activeScene.restart()
        self.unpause()
        return

    def returnToMainMenu(self):
        self.activeScene = self.menu_scene
        self.is_paused = False
        return;

    def exit(self):
        pygame.event.post( pygame.event.Event( pygame.QUIT ) )
        return