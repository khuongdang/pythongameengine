from Object.Object import Object
from Object.Circle import Circle
from Object.Line import Line
from Math.Vec2 import Vec2
from Object.Polygon import Polygon
from Object.ObjectsGroup import ObjectsGroup
import pygame
from Math import Math
import math
from Math import Utility
import Config
from Color import Color
#################
#
# You only need to call ResolveCollision, the function will automatically works through all the case ^^
#
# If you call others function, there will be no verification, so becareful
#
##################

class CollisionDetect:
    @staticmethod
    def isColided( A, B ):
        if( isinstance(A, Circle ) & isinstance(B, Circle) ):
            return CollisionDetect.isColided_Circle_Circle(A,B)
        ##################
        if( isinstance(A, Circle) & isinstance(B,Line ) ):
            return CollisionDetect.isColided_Circle_Line(A,B)
        if( isinstance(A, Line) & isinstance(B,Circle ) ):
            return CollisionDetect.isColided_Circle_Line(B,A)
        ###################
        if( isinstance(A, Circle ) & isinstance(B, Polygon) ):
            return CollisionDetect.isColided_Circle_Polygon(A,B)
        if( isinstance(A, Polygon ) & isinstance(B, Circle) ):
            return CollisionDetect.isColided_Circle_Polygon(B,A)
        return False

    @staticmethod
    def isColided_Circle_Circle(A,B):
        if not (  isinstance(A, Circle ) & isinstance(B, Circle) ):
            return False
       
        A_r = A.radius
        B_r = B.radius
        dis = Math.DistanceFromPoints( A.position, B.position )
        if ( dis <= A_r + B_r ):
            return True
        return False

    @staticmethod
    def isColided_Circle_Line(circle,line):
        if not ( isinstance(circle, Circle) & isinstance(line,Line ) ):
            return False

        c_r = circle.radius
        l_r = line.radius

        distance = Math.DistanceFromPointToLine(circle.position , line)
        if ( (0.0 <= distance) and (distance <= (c_r+l_r)) ):
                return True
        return False

    @staticmethod   
    def isColided_Line_Polygon(line, polygon):
        if not ( isinstance(line, Line) & isinstance(polygon,Polygon ) ):
            return False

        return True

    @staticmethod
    def isColided_Circle_Polygon(circle,polygon):
        
        if not ( isinstance(circle, Circle) & isinstance(polygon,Polygon ) ):
            return False
        
        ####Broad phase:
        min_length = circle.radius + polygon.bounding_radius
        polygon_circle_vec = Math.GetVecFromPoints(polygon.position,circle.position)
        polygon_circle_length = polygon_circle_vec.magnitude()
        if polygon_circle_length > min_length :
            return False
        ######
        
        shortest_overlap_dis = float("inf")
        shortest_overlap_direction = 0
        shortest_overlap_vec = None
        reflect_line = None
        
        for i in range(-1, len(polygon.vertexs)-1 ):
            line_tuple = ( polygon.getVertexPos(i) , polygon.getVertexPos(i+1) )
            line_vec = Math.GetVecFromPoints( line_tuple[0] , line_tuple[1]  )
            line_circle_vec = Math.GetVecFromPoints( line_tuple[0] , circle.position )

            center_projection = line_circle_vec.dot( line_vec.normalize() )

            max_range = center_projection + circle.radius
            min_range = center_projection - circle.radius

            if ( min_range >= line_vec.magnitude() ):
                return False
            if ( max_range <= 0 ):
                return False

            temp_overlap_min = line_vec.magnitude() - min_range
            temp_overlap_max = max_range
            if ( temp_overlap_min <= temp_overlap_max ):
                temp_shortest_overlap_dis = temp_overlap_min
                temp_shortest_overlap_direction = 1
            else:
                temp_shortest_overlap_dis = temp_overlap_max
                temp_shortest_overlap_direction = -1

            if (temp_shortest_overlap_dis < shortest_overlap_dis):
                shortest_overlap_dis = temp_shortest_overlap_dis + 0.9
                shortest_overlap_direction = temp_shortest_overlap_direction
                reflect_line = line_vec

        shortest_overlap_vec = reflect_line.normalize() * shortest_overlap_direction * shortest_overlap_dis
        # from App.GameManager import GameManager
        # GameManager.getInstance().addRenderQueue( "Line", ( circle.position, circle.position + shortest_overlap_vec ) )
        # GameManager.getInstance().pause()

        return shortest_overlap_vec, reflect_line
        ####

class CollisionResponde:
    class StaticResolve:
        @staticmethod
        def Resolve(A,B,param = []):
            if( isinstance(A, Circle ) & isinstance(B, Circle) ):
                return CollisionResponde.StaticResolve.Circle.Circle.Resolve(A,B)
            ################
            if( isinstance(A, Circle ) & isinstance(B, Line) ):
                return CollisionResponde.StaticResolve.Circle.Line.Resolve( A,B )
            if( isinstance(A, Line ) & isinstance(B, Circle) ):
                return CollisionResponde.StaticResolve.Circle.Line.Resolve( B,A )
            #################
            if( isinstance(A,Circle) & isinstance(B,Polygon) ):
                return CollisionResponde.StaticResolve.Circle.Polygon.Resolve(A,B, param)
            if( isinstance(A,Polygon) & isinstance(B,Circle) ):
                return CollisionResponde.StaticResolve.Circle.Polygon.Resolve(B,A, param)
            ########### 
            return False

        class Circle:
            class Polygon:
                @staticmethod
                def Resolve(circle, polygon, param):
                    if not (  isinstance(circle, Circle ) & isinstance(polygon, Polygon) ):
                        return False

                    #shift_line = -param + Vec2( circle.radius, circle.radius ) * 2
                    # shift_line = Math.GetVecFromPoints( circle.position, polygon.position )
                    circle.shift( param[0] )

                    return param[1:]
            class Line:
                @staticmethod
                def Resolve(circle, line):
                    if not (  isinstance(circle, Circle ) & isinstance(line, Line) ):
                        return False

                    point_line_vec = Math.GetVecFromPointToLine( circle.position, line )
                    if( point_line_vec == (0.0,0.0) ):
                        return False
                    
                    distance = point_line_vec.magnitude()
                    #added 0.9 to stop the ball from sticking to the line
                    dis_to_move = ((circle.radius + line.radius) - distance) + 0.1
                    line_point_nor = (-point_line_vec).normalize()
                    circle.shift( line_point_nor*dis_to_move )

                    return True
            class Circle:
                @staticmethod
                def Resolve(A,B):
                    if not (  isinstance(A, Circle ) & isinstance(B, Circle) ):
                        return False

                    vec_A_B = Math.GetVecFromPoints( A.position , B.position )
                    if(vec_A_B == Vec2.Zero()):
                        vec_A_B = Vec2(1,0)
                    dis_A_B =  vec_A_B.magnitude()
                    #added 0.9 to stop the ball from sticking to other ball
                    dis_to_move = (((A.radius + B.radius) - dis_A_B)/2 ) + 0.9
                    
                    if ( not A.is_movable):
                        vec_move_B = vec_A_B.normalize() * dis_to_move * 2
                        vec_move_A = Vec2.Zero()
                    elif (not B.is_movable):
                        vec_move_B = Vec2.Zero()
                        vec_move_A = -vec_A_B.normalize() * dis_to_move * 2       
                    else:
                        vec_move_B = vec_A_B.normalize() * dis_to_move
                        vec_move_A = -vec_move_B

                    A.shift(vec_move_A)
                    B.shift(vec_move_B)
                    
                    return True

    class DynamicResolve:
        @staticmethod
        def Resolve(A,B,param = []):
            if( isinstance(A, Circle ) & isinstance(B, Circle) ):
                return CollisionResponde.DynamicResolve.Circle.Circle.Resolve(A,B)
            ################
            if( isinstance(A, Circle ) & isinstance(B, Line) ):
                return CollisionResponde.DynamicResolve.Circle.Line.Resolve( A,B )
            if( isinstance(A, Line ) & isinstance(B, Circle) ):
                return CollisionResponde.DynamicResolve.Circle.Line.Resolve( B,A )
            #################
            if( isinstance(A,Circle) & isinstance(B,Polygon) ):
                return CollisionResponde.DynamicResolve.Circle.Polygon.Resolve(A,B, param)
            if( isinstance(A,Polygon) & isinstance(B,Circle) ):
                return CollisionResponde.DynamicResolve.Circle.Polygon.Resolve(B,A, param)
            ########### 
            return False
            
        
        class Circle:
            class Polygon:
                @staticmethod
                def Resolve(circle, polygon, param):
                    if not (  isinstance(circle, Circle ) & isinstance(polygon, Polygon) ):
                        return False

                    if( polygon.is_movable ):
                        return True
                    else:
                        line_vec = param[0]
                        circle.reflect( line_vec )        
                        circle.touched()
                        pass
                    
                    return True
            class Line:
                @staticmethod
                def Resolve(circle, line):
                    if not (  isinstance(circle, Circle ) & isinstance(line, Line) ):
                        return False

                    cir_to_nearest = Math.GetVecFromPointToLine( circle.position, line )
                    if(cir_to_nearest == (0.0,0.0)):
                        return False
                    circle.reflect( cir_to_nearest )
                    circle.touched()

                    return True

            class Circle:
                @staticmethod
                def Resolve(A,B):
                    if not (  isinstance(A, Circle ) & isinstance(B, Circle) ):
                        return False

                    if ( not A.is_movable and B.is_movable ):
                        Utility.Swap(A,B)

                    vec_A_B = Math.GetVecFromPoints( A.position , B.position )

                    normal_vec_A_B = vec_A_B.normalize()
                    tangent_vec = normal_vec_A_B.rotate(90)

                    if ( A.is_movable and B.is_movable ):
                        #https://stackoverflow.com/questions/35211114/2d-elastic-ball-collision-physics
                        momentum_A = A.velocity.dot( normal_vec_A_B ) - (2.0 * B.mass * (A.velocity - B.velocity).dot(normal_vec_A_B))/(A.mass+B.mass)
                        momentum_B = B.velocity.dot( normal_vec_A_B ) - (2.0 * A.mass * (B.velocity - A.velocity).dot(normal_vec_A_B))/(A.mass+B.mass)

                        A.velocity = Vec2 ( (tangent_vec * A.velocity.dot( tangent_vec )) + (momentum_A * normal_vec_A_B) )
                        B.velocity = Vec2 ( (tangent_vec * B.velocity.dot( tangent_vec )) + (momentum_B * normal_vec_A_B) )

                        A.touched()
                        B.touched()
                        return True
                    else:
                        A.reflect(normal_vec_A_B)
                        A.touched()
                        #A.velocity = (tangent_vec * A.velocity.dot( tangent_vec )) - ( normal_vec_A_B * A.velocity.dot( normal_vec_A_B ) )             
                        return True




    @staticmethod
    def ResolveCollision(A,B):
        if ( not A.is_movable and not B.is_movable):
            return False

        ######## Deal with objectsgroup
        if ( isinstance( A, ObjectsGroup ) ):
            for _object in A.objects:
                CollisionResponde.ResolveCollision( _object, B )
        if ( isinstance( B, ObjectsGroup ) ):
            for _object in B.objects:
                CollisionResponde.ResolveCollision( A, _object )      
        ########          

        colided_result = CollisionDetect.isColided(A,B)
        if( colided_result ):
            static_result = CollisionResponde.StaticResolve.Resolve(A,B, colided_result)
            if( static_result ):
                dynamic_result = CollisionResponde.DynamicResolve.Resolve(A,B, static_result)
                return dynamic_result
            # CollisionResponde.StaticResolveCollision( A,B )
            # CollisionResponde.DynamicResolveCollision( A,B )
        return False


    