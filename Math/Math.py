import pygame
from Math.Vec2 import Vec2
import math
PI = 3.14159265359

def Square(a):
    return a*a

def DistanceFromPoints( A , B):
    if( isinstance(A , Vec2 ) & isinstance(B,  Vec2) ):
        return math.sqrt( Square( A.x - B.x ) + Square(A.y - B.y) )
    return 0.0

def GetVecFromPoints( A,B ):
    if( isinstance(A , Vec2 ) & isinstance(B,  Vec2) ):
        return Vec2( B.x-A.x , B.y-A.y )
    return Vec2.Zero()

def DistanceFromPointToLine(point , line):
    from Object.Line import Line
    if( isinstance(point , Vec2 ) & isinstance( line, Line ) ):
        return GetVecFromPointToLine( point,line ).magnitude()
    if( isinstance(point , Vec2 ) & isinstance( line, tuple ) ):
        return GetVecFromPointToLine( point,line ).magnitude()
    return -1.0

def GetVecFromPointToLine(point, line):

    def GetVecFromPointToTuple(point, line):
        line_vec = GetVecFromPoints( line[0], line[1] )
        point_to_first_vec = GetVecFromPoints(point, line[0])
        closet_point_vec_mag = (-point_to_first_vec).dot( line_vec.normalize() )

        if  closet_point_vec_mag < 0.0:
            closet_point_vec_mag = 0.0
        elif closet_point_vec_mag > line_vec.magnitude():
            closet_point_vec_mag = line_vec.magnitude()

        closet_point_vec = ( line_vec.normalize() * closet_point_vec_mag )
        result = point_to_first_vec + closet_point_vec
        return result

    from Object.Line import Line
    if( isinstance(point , Vec2 ) & isinstance( line, Line ) ):
        return GetVecFromPointToTuple( point, ( line.first_point, line.second_point ) )
    elif( isinstance(point , Vec2 ) & isinstance( line, tuple ) ):
        return GetVecFromPointToTuple( point, line )
    return Vec2.Zero()