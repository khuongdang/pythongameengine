import pygame

class Vec2( pygame.math.Vector2 ):
    def __init__(self, width =0.0, height=0.0):
        super().__init__(width,height)

    def reflect(self, normal):
        return Vec2( super( Vec2, self ).reflect(normal) )

    def friction(self, strength):
        result = Vec2(self)
        abs(strength)
        if( (strength >= 0.00001) and (result != Vec2.Zero() )):
            if ( result.magnitude() - strength >= 0.0001 ):
                result -= result.normalize() * strength
            else:
                result = Vec2.Zero()
        return result

    @staticmethod
    def Zero():
        return Vec2(0.0,0.0)

    def toTupleInt(self):
        return [ int(self.x), int(self.y) ]

    def rotate(self,degree):
        return Vec2( super(Vec2, self).rotate(degree) )

    def normalize(self):
        return Vec2( super(Vec2, self).normalize() )

    def __add__(self,other):
        x = self.x + other.x
        y = self.y + other.y
        return Vec2(x,y)

    def __sub__(self,other):
        x = self.x - other.x
        y = self.y - other.y
        return Vec2(x,y)

    def __mul__(self,scala):
        x = self.x * scala
        y = self.y * scala
        return Vec2(x,y)

    def __truediv__(self,scala):
        x = self.x / scala
        y = self.y / scala
        return Vec2(x,y)

    def __eq__(self, value):
        return super().__eq__(value)

    def __ne__(self, value):
        return super().__ne__(value)