import pygame
from Color import Color
from Math.Vec2 import Vec2

class GUIElement(object):
    def __init__(self):
        super().__init__()

        self.position = Vec2()
        self.size = None
        self.surface = pygame.display.get_surface()
        self.camera = None
        self.text = None
        self.text_color = Color.BLACK
        self.font = 'Time New Roman'
        self.onclick = None

    def onClick(self,x,y):
        return

    def setFont(self, font):
        self.font = font
        return

    def setTextColor(self, color):
        self.text_color = color
        return

    def setCamera(self, camera):
        self.camera = camera
        return self.camera

    def isClicked(self, pos):
        return NotImplementedError

    def render(self):
        return NotImplementedError

    def renderText(self, size = 30, align = "center"):
        if(self.text is not None):
            size = int(size)
            pygame.font.init()
            myfont = pygame.font.SysFont(self.font,size)
            textsurface = myfont.render(self.text,True,(255,255,255,100))
            surface_size = myfont.size(self.text)

            if (align == "left"):
                pos = ( self.position.x  , self.position.y - surface_size[1]/2 )
            elif (align == "right"):
                pos = ( self.position.x - surface_size[0]  , self.position.y - surface_size[1]/2 )
            else:
                pos = ( self.position.x - surface_size[0]/2  , self.position.y - surface_size[1]/2 )

            self.surface.blit( textsurface, pos )