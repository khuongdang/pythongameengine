import pygame
import Config
from UI.GUIElement import GUIElement
from Math.Vec2 import Vec2

class Button(GUIElement):
    def __init__(self, color, text, pos = Vec2() , size = Vec2()):
        super().__init__()

        self.position = pos
        self.size = size
        self.color = color
        self.border_color = self.color
        self.text = text
        self.text_size = int(self.size.y/3)
        self.onclick = None
        self.highlight_color = self.color + pygame.Color(40,40,40,0)
        self.highlight_border_color = self.color + pygame.Color(40,40,40,0)
        self.is_highlighting = False
        self.is_disabled = False
        self.disabled_color = Config.DISABLED_BUTTON_COLOR

    def highlight(self, boolean = True):
        self.is_highlighting = boolean
        return

    def disable(self, boolean = True):
        self.is_disabled = boolean
        return
    
    def onHover(self, x, y):
        if not self.is_disabled:
            self.highlight( self.isInside(x,y) )
            return


    def onClick(self, x, y):
        if not self.is_disabled:
            if self.isInside(x,y):
                if (self.onclick is not None):
                    self.onclick()
        return

    def isInside(self, x, y):
        X_ = (self.position.x - self.size.x/2 < x) and (self.position.x + self.size.x/2 > x) 
        Y_ = (self.position.y - self.size.y/2 < y) and (self.position.y + self.size.y/2 > y) 
        if (X_ and Y_):
            return True
        return False

    def setBorderColor(self, color):
        self.border_color = color
        return

    def render(self):
        if ( self.camera is None ):
            scroll = Vec2(0,0)
        else :
            scroll = self.camera.getScroll()

        color = self.color
        border_color = self.border_color

        if(self.is_highlighting):         
            color = self.highlight_color
            border_color = self.highlight_border_color
        elif(self.is_disabled):
            color = self.disabled_color
            border_color = self.disabled_color

        rect = pygame.Rect(self.position - self.size/2 ,self.size)
        pygame.draw.rect(self.surface, color, rect)
        pygame.draw.rect(self.surface, border_color, rect, Config.BUTTON_BORDER_WIDTH )
        
        self.renderText( size = self.text_size )
        return