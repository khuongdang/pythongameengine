import pygame

BLACK = pygame.Color( 0, 0, 0)
WHITE =  pygame.Color( 255, 255, 255)
GREEN =  pygame.Color( 0, 255, 0)
RED =  pygame.Color( 255, 0, 0)
CLEAR =  pygame.Color( 0, 0, 0, 0)
GRAY = pygame.Color( 100,100,100 )