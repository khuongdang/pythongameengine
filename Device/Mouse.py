import pygame
import Config
from Math.Vec2 import Vec2

class Mouse:
    def __init__(self):
        self.leftMouse = False
        self.middleMouse = False
        self.rightMouse = False
        self.lastLeftMouse = False
        self.lastRightMouse = False
        self.pos = Vec2(Config.width/2, Config.height/2)

    def isLeftMouseUp(self):
        if ( self.lastLeftMouse == True and self.leftMouse == False ):
            return True
        return False

    def isRightMouseUp(self):
        if ( self.lastRightMouse == True and self.rightMouse == False ):
            return True
        return False

    def update(self):
        self.lastLeftMouse = self.leftMouse
        self.lastRightMouse = self.rightMouse
        self.leftMouse, self.middleMouse, self.rightMouse = pygame.mouse.get_pressed()   
        self.pos = Vec2(pygame.mouse.get_pos())

    