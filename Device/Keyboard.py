import pygame

class Keyboard:
    def __init__(self):
        self.stages = pygame.key.get_pressed()
        self.last_stage = pygame.key.get_pressed()

    def update(self):
        self.last_stage = tuple(self.stages)
        self.stages = pygame.key.get_pressed()
        pass

    def isDDown(self):
        return self.stages[ pygame.K_d ]

    def isADown(self):
        return self.stages[ pygame.K_a ]

    def isESCDown(self):
        return self.stages[ pygame.K_ESCAPE ]

    def isESCUp(self):
        if ( self.last_stage[ pygame.K_ESCAPE ] ):
            return not (self.stages[ pygame.K_ESCAPE ])
        return False
