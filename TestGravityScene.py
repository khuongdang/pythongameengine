import pygame
from App.GameScene import GameScene
from Object.Circle import Circle
from Object.Object import Object
from Object.Line import Line
from Math.Vec2 import Vec2
from Object.Polygon import Polygon
from Object.Joint import Joint
from Projects.HillDriving.Car import Car
from Color import Color
from Math import Math
import math
import random
import datetime
from Math import PhysicWorker
import Config

class TestGravityScene(GameScene):
    def __init__(self):
        super().__init__()
        random.seed( datetime.datetime.now() )
         
        # for i in range(0,20):   
        #     circle = Circle( ( random.randint(0,255),random.randint(0,255) ,random.randint(0,255)  ), random.randint(0,800) , random.randint(0,800) , 1 )
        #     circle.velocity = Vec2.Zero()
        #     circle.accelerator = Vec2.Zero()
        #     circle.gravity = Config.GRAVITY
        #     self.addObject( circle )

        # for i in range(0,5):
        #     circle = Circle( ( random.randint(0,255),random.randint(0,255) ,random.randint(0,255)  ), random.randint(0,800) , random.randint(0,800) , random.randint(10,30) )
        #     circle.is_movable = False
        #     self.addObject(circle)


        line = Line(Color.RED, 0,800,1200,800)  
        self.addObject(line)
        line = Line(Color.RED, 0,0,0,800)  
        self.addObject(line)
        line = Line(Color.RED, 1200,0,1200,800)  
        self.addObject(line)
        line = Line(Color.RED, 0,0,1200,0)  
        self.addObject(line)

        line = Line(Color.WHITE, 0,800,600,700)  
        self.addObject(line)
        line = Line(Color.WHITE, 600,700,1200,800)  
        self.addObject(line)

        self.car = Car(Color.WHITE, 400,500)
        self.addObject(self.car )

        # line = Line(Color.RED, 600, 600,800,100)
        # self.addObject(line)
        # line = Line(Color.RED, 100,100,300,200,20)
        # self.addObject(line)

        # vertexs = [ Vec2(-100,-100), Vec2(100,-100), Vec2(100,100), Vec2(-100,100) ]
        # poligon = Polygon(Color.RED, 350,450, vertexs)
        # poligon.is_movable = False
        # poligon.angle = 10
        # poligon.angle_velocity = 10
        # self.addObject(poligon)
        pass

    def update(self,duration):

        # for _object in self.objects_array:
        #     _object.accelerator = Vec2( 0, 0 )   
        super().update(duration)

        for i in range( len(self.objects_array) ):
            for j in range (i+1 , len(self.objects_array) ):
                PhysicWorker.CollisionResponde.ResolveCollision( self.objects_array[i], self.objects_array[j] )

        pass
    
    def render(self):
        super().render()
        pass

    def onClick(self, x, y):
        self.car.moveRight()

    def onHoldLeftMouse(self,x,y):
        pass

    def onHoldRightMouse(self,x,y):
        pass

    def onDDown(self):
        self.car.moveRight()

    def onADown(self):
        self.car.moveLeft()